#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

MTKSEPOLICY_PATH := device/xiaomi/dandelion-mtk-sepolicy

BOARD_VENDOR_SEPOLICY_DIRS += \
    $(MTKSEPOLICY_PATH)/basic/non_plat \
    $(MTKSEPOLICY_PATH)/bsp/non_plat \
    $(MTKSEPOLICY_PATH)/bsp/non_plat/ota \
    $(MTKSEPOLICY_PATH)/full/non_plat

BOARD_PLAT_PUBLIC_SEPOLICY_DIR += \
    $(MTKSEPOLICY_PATH)/basic/plat_public \
    $(MTKSEPOLICY_PATH)/bsp/plat_public \
    $(MTKSEPOLICY_PATH)/full/plat_public

BOARD_PLAT_PRIVATE_SEPOLICY_DIR += \
    $(MTKSEPOLICY_PATH)/basic/plat_private \
    $(MTKSEPOLICY_PATH)/bsp/plat_private \
    $(MTKSEPOLICY_PATH)/full/plat_private

